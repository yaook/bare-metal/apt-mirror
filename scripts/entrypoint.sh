#!/bin/bash

# entrypoint.sh for the docker container used to build the mirror

python3 scripts/nginx-conf-gen.py cfg.yaml
chown `stat -c "%u:%g" /home` nginx.conf
sed -i 's#/.*$##' pkglists/*
mkdir -p temp_mirror_dir/
python3 scripts/custom_downloads.py cfg.yaml temp_mirror_dir
python3 scripts/apt-mirror-gen.py cfg.yaml pkglists/ temp_mirror_dir
tar cfz apt-mirror.tar.gz temp_mirror_dir/
chown `stat -c "%u:%g" /home` apt-mirror.tar.gz
rm -r temp_mirror_dir/
