#!/usr/bin/python3

# downloads packages defined via custom_downloads section in cfg.yaml
# usage: ./scripts/custom_downloads.py cfg.yaml [<target_dir>]

import sys
import os
import yaml
import wget
from pathlib import Path

with open(sys.argv[1]) as f:
    cfg = yaml.safe_load(f)

enabled = bool(cfg['custom_downloads_enabled']) if 'custom_downloads_enabled' in cfg \
            else False
if not enabled:
    sys.exit()

target_dir = sys.argv[2] if len(sys.argv) > 2 else ""

for repo_name, files in cfg["custom_downloads"].items():
    if repo_name not in cfg['repos']:
        print("No repo definition found for", repo_name, "custom_downloads, skipping.")
        continue
    for file in files:
        repo = cfg['repos'][repo_name]
        file_path = repo['path'] + '/' + file
        url = 'http://' + repo['hosts'][0] + file_path
        download_path = target_dir + '/' + repo_name + file_path
        Path(os.path.dirname(download_path)).mkdir(parents=True, exist_ok=True)
        if os.path.exists(download_path):
            os.remove(download_path)  # if file exists already, remove it
        wget.download(url, download_path)
