#!/usr/bin/python3

# script that assembles the nginx.conf file to be used for serving the mirror via nginx
# usage: ./scripts/nginx-conf-gen.py cfg.yaml

import sys
import pathlib
import yaml
import crossplane

with open(sys.argv[1]) as f:
    cfg = yaml.safe_load(f)

nginx_port = str(cfg['nginx']['port']) if 'nginx' in cfg and 'port' in cfg['nginx'] \
             else '80'
mirror_path = cfg['nginx']['mirror_path'] \
              if 'nginx' in cfg and 'mirror_path' in cfg['nginx'] \
              else '/usr/share/mirror'
config = crossplane.build([
    {
        "directive": "user",
        "args": ["www-data"]
    },
    {
        "directive": "worker_processes",
        "args": ["auto"]
    },
    {
        "directive": "pid",
        "args": ["/run/nginx.pid"]
    },
    {
        "directive": "events",
        "args": [],
        "block": [{
            "directive": "worker_connections",
            "args": ["1024"]
        }]
    },
    {
        "directive": "http",
        "args": [],
        "block": [{
            "directive": "server",
            "args": [],
            "block": [{
                "directive": "server_name",
                "args": repo["hosts"]
                },
                {
                "directive": "listen",
                "args": [nginx_port]
                },
                {
                "directive": "location",
                "args": [repo["path"]],
                "block": [{
                    "directive": "root",
                    "args": [str(pathlib.Path(mirror_path) / pathlib.Path(repo_name))]
                }]
            }]
        } for repo_name, repo in cfg["repos"].items()]
    }
])

with open("nginx.conf", "w") as file:
    file.write(config + "\n")
