#!/usr/bin/python3

# usage: ./scripts/apt-mirror-gen.py cfg.yaml pkglist [<target_dir>]

import os
import pathlib
import tempfile
import subprocess
import sys
import yaml


# put package list into a trie structure and add the version pins from cfg.yaml
def compress_package_list(pkgs, to=None, version_pins=None):
    trie_root = {} if to is None else to
    version_pins = {} if version_pins is None else version_pins

    for line in pkgs:
        line = line.strip()
        if not line:
            continue
        if line.startswith("#"):
            continue

        cur = trie_root
        for letter in line:
            cur = cur.setdefault(letter, {})
        cur[None] = ''

        if line in version_pins:
            cur[None] = '(_' + version_pins[line] + ')'

    return trie_root


def serialize_trie_to_regex(trie):
    def escape(letter):
        if letter.isalnum() or letter in '-':
            return letter
        else:
            return '\\' + letter

    if len(trie) == 0:
        raise Exception("Invalid trie – no stop marker")

    elif len(trie) == 1:
        (letter, subtrie), = trie.items()
        if letter is None:
            # end of a sequence
            return subtrie
        else:
            return escape(letter) + serialize_trie_to_regex(subtrie)

    else:
        return '(' + '|'.join(
            subtrie if letter is None
            else escape(letter) + serialize_trie_to_regex(subtrie)
            for letter, subtrie in trie.items()
        ) + ')'


with open(sys.argv[1]) as f:
    cfg = yaml.safe_load(f)

# build trie from package-lists
pkg_trie = {}
for fn in pathlib.Path(sys.argv[2]).glob("*.list"):
    with open(fn, "rt") as f:
        compress_package_list(f, to=pkg_trie, version_pins=cfg.get("version_pins"))

# build regex from trie
file_regex = ('/' + serialize_trie_to_regex(pkg_trie) +
              '_[^/]*\\.deb' + '|/Translation-(de|en).*|cnf|dep11')

target_dir = sys.argv[3] if len(sys.argv) > 3 else ""

# call debmirror for all repos in the cfg.yaml using the previously built regex
for repo_name, repo in cfg["repos"].items():
    if repo.get("ignore", False):
        continue
    with tempfile.NamedTemporaryFile("r+") as keyring:
        subprocess.run(
            ["gpg", "--no-default-keyring", "--keyring", keyring.name, "--import", "-"],
            input=repo["keyring"].encode("us-ascii"),
            # check=True, # when settig this things don't work
        )

        # keyring.write(repo["keyring"])

        # We hold a frozen state of the debmirror script in this repo
        # to be safe from upstream shenanigans. Switch to using it here, if so desired
        call = ["debmirror", "-v", "--progress"]
        # call = ["scripts/debmirror_custom.pl", "-v", "--progress"]

        call += ["-a", "amd64"]
        call += ["--i18n"]
        call += ["--method", "http"]
        call += ["-r", repo["path"]]
        call += ["--keyring", keyring.name]

        # experimental option to try if it helps stability
        call += ["--retry-rsync-packages=2"]

        # we skip downloading project/trace/* files as some repos (download.docker.com)
        # don't have them which results in the debmirror call not working
        call += ["--rsync-extra=none"]

        if "dist" in repo:
            call += ["-d", repo["dist"]]

        if "sections" in repo:
            call += ["-s", ','.join(repo["sections"])]

        if repo.get("source", False):
            call += ["--source"]
        else:
            call += ["--no-source"]

        call += ["-h", repo["hosts"][0]]

        call += ["--exclude", "/.*"]
        call += ["--include", file_regex]

        call += [pathlib.Path(target_dir) /
                 pathlib.Path(repo_name) / repo["path"].lstrip(os.path.sep)]

        # subprocess.run(call, check=True)
        # can't use check=True because debmirror might return exit code 2
        # which indicates something like "no changes"
        result = subprocess.run(call)
        if result.returncode not in [0, 2]:
            result.check_returncode()
