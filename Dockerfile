FROM ubuntu:22.04

RUN DEBIAN_FRONTEND=noninteractive apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-suggests --no-install-recommends debmirror gnupg python3 python3-pip; \
    pip3 install pyyaml crossplane wget

# fix a bug with repos without root directory (e.g. apt.kubernetes.io and apt.releases.hashicorp.com)
# see https://bugs.launchpad.net/ubuntu/+source/debmirror/+bug/1702059
RUN sed -i 's#$remoteroot/#$remoteroot#g' /usr/bin/debmirror; \
    sed -i 's#${remoteroot}/#${remoteroot}#g' /usr/bin/debmirror; \
    sed -i '/^$remoteroot =~.*/i $remoteroot =~ s|\/?$|\/| ;' /usr/bin/debmirror

COPY --chmod=700 scripts/entrypoint.sh /entrypoint.sh

WORKDIR /home
ENTRYPOINT ["/entrypoint.sh"]
