The partial mirror can be used to enable a proper offline-installation of a YAOOK cluster.

If you want to create an apt cache which pulls stuff on demand, look at apt-cacher-ng.
If you want to create complete mirrors, look at ftpmirror or similar.

# 1. Collecting lists of packages

Currently, we need an existing functioning cluster to collect a list of all the required apt packages for offline installation. Effectively an "online" cluster is needed at first. A list of all installed APT packages is generated via `apt list --installed`. That command has to be issued on one node of each type. Those types of nodes are:

* K8s master
* K8s worker
* Yaook Metal-Controller

Additionally, you may need packages for a temporary bootstap-machine, which we will call

* install-node

Since the collection method leaves a list that's more verbose than needed, you can simplify them via `sed -i 's#/.*$##' pkglists/*`


# 2. Preparing cfg.yaml

## Version pins in cfg.yaml

We pin vault, because without a pin we have >9GB of vault packages in the mirror, which more than doubles the size of the whole thing. The deploy.sh of yaook-base does not have a version pin, so you need to pin the latest version that is found in the upstream hashicorp repo (or adapt the deploy.sh).
For reference you can also check the Yaook k8s CI for the vault docker tag pinned by the renovate bot (but it may be lagging behind a bit): https://gitlab.com/yaook/k8s/-/blob/devel/.gitlab-ci.yml#L322

We can use regex for the version pins. Examples:
```
vault: 1\.15\.2-1
vault: 1\.15\..*
```

## Non-conforming repos

The debmirror tool is too strict in it's assumptions about how repos are structured. It requires the structure `http://apt.example.com/$remoteroot/dists/@dists` (see https://bugs.launchpad.net/ubuntu/+source/debmirror/+bug/1977623) to find `Release` files, and `$section/binary-$arch/$file` from there to the `Packages` files. The remoteroot can be defined as empty when using a simple patch (included in the Dockerfile, and explained in the manual way), but too much other stuff is hardcoded. So some repos like 
`https://linux.mellanox.com/public/repo/` and `https://pkgs.k8s.io` can't be mirrored via this tool. We still have the mirror defined in the cfg.yaml for the purpose of the nginx.conf generation, and use `ignore: true` to skip it in the mirror creation.

## Custom_downloads

A possible workaround to deal with non-conforming repos is to download the required files manually, or semi-manually via defining the required stuff in the `custom_downloads` section.
This is effectively just a wget for every defined file done by `custom_downloads.py`. It takes the host + path from an equally named repo definition in the same cfg.yaml (this ensures the generated nginx config fits the download path).

The versions of `kubectl`, `kubeadm` and `kubelet` need to match the kubernetes version that will be used (which will be defined in the config/config.toml of your cluster repo).
You can check https://gitlab.com/yaook/k8s/-/blob/devel/templates/config.template.toml for the most recent k8s version, something like
```
[kubernetes]
# Kubernetes version. Currently, we support from 1.24.* to 1.26.*.
version = "1.26.8" # •ᴗ•
```

The versions of `cri-tools` and `kubernetes-cni` should probably be the most recent ones (of the k8s repo matching the desired k8s release).

The k8s repos in `https://pkgs.k8s.io/` can't be viewed via browser (they have a CDN and give access denied for directories). But they can be checked for example via:
https://download.opensuse.org/repositories/isv:/kubernetes:/core:/stable:/v1.24/deb/


# 3. Creating the partial mirror

The main star is the apt-mirror-gen.py script, which uses debmirror under the hood.

The script expects the packagelists ending on .list, and them being a simple list of the package names, e.g.
```
$ cat pkglists/example.list
packageA
packageB
```

## via Docker-Image

Put the package lists into a `pkglists/` directory.
Run these in the repo directory:

```
docker build -t apt-mirror-creator .
docker run -v /$(pwd):/home -it apt-mirror-creator
```

The packed mirror and the nginx.conf should be in your working directory now.

Note: custom_download for the linux-mellanox.com repo might not work if you are running docker inside a Openstack VM (they probably have some bad MTU config), unless you explicitely set the docker MTU (e.g. to 1450 or smaller, depending on your Openstack setup) in /etc/docker/daemon.json and restart docker.

## the manual way

Hint: The apt-mirror-gen.py script might screw up your trustdb. I recommend to use a VM or a docker container.

* `mkdir pkglists`
* copy package lists and the python scripts from this repo to your target VM or build place of choice.
* `sudo apt install debmirror gnupg python3`
* patch debmirror
  * to fix a bug with repos without root directory (e.g. apt.kubernetes.io and apt.releases.hashicorp.com)
  * see https://bugs.launchpad.net/ubuntu/+source/debmirror/+bug/1702059
  ```
  sudo cp /usr/bin/debmirror /usr/bin/debmirror.old
  sudo sed -i 's#$remoteroot/#$remoteroot#g' /usr/bin/debmirror
  sudo sed -i 's#${remoteroot}/#${remoteroot}#g' /usr/bin/debmirror
  sudo sed -i '/^$remoteroot =~.*/i $remoteroot =~ s|\/?$|\/| ;' /usr/bin/debmirror
  ```
* `./scripts/apt-mirror-gen.py cfg.yaml pkglists/ <target-dir>`
* `./scripts/custom_downloads.py cfg.yaml <target-dir>`
* `tar cfz apt-mirror.tar.gz <mirror-dir>`
* Generate nginx config
  ```
  sudo apt install python3-pip
  pip install pyyaml crossplane
  ./nginx-conf-gen.py cfg.yaml
  ```


# 4. Serving the mirror

You can use the standard nginx docker image and just mount the nginx.conf and the data.

```
sudo docker run --net=host --name apt-mirror -d -v ~/nginx.conf:/etc/nginx/nginx.conf:ro -v /usr/share/mirror/:/usr/share/mirror/:ro  nginx:1.25
```


# 5. Using the mirror

/etc/apt/apt.conf.d/00proxy
```
Acquire::http::Proxy "http://<target>:3142/";
```

Make sure /etc/apt/sources.list has the correct entries.
This means it references all the repos you defined in cfg.yaml but not others (that would result in error messages during apt update).

The default content of sources.list:
```
deb http://archive.ubuntu.com/ubuntu jammy main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu jammy-updates main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu jammy-backports main restricted universe multiverse

deb http://security.ubuntu.com/ubuntu jammy-security main restricted universe multiverse
```
And the additional repos:
```
deb [signed-by=/etc/apt/keyrings/kubernetes.gpg] http://pkgs.k8s.io/core:/stable:/v1.24/deb/ /

deb [arch=amd64 signed-by=/etc/apt/keyrings/hashicorp-archive-keyring.gpg] http://apt.releases.hashicorp.com/ jammy main

deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] http://download.docker.com/linux/ubuntu jammy stable

deb [signed-by=/etc/apt/keyrings/mellanox.gpg] http://linux.mellanox.com/public/repo/mlnx_ofed/5.8-1.1.2.1/ubuntu22.04/amd64 ./

```
Make sure you have the gpg public keys for the non-ubuntu repositories.

```
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.24/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes.gpg

wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/hashicorp-archive-keyring.gpg

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

wget -qO - https://www.mellanox.com/downloads/ofed/RPM-GPG-KEY-Mellanox | sudo gpg --dearmor -o /etc/apt/keyrings/mellanox.gpg
```

Then use apt.
Remember that you will see every package of a repo according to the Release and Package files, but can only install those that were actually added to the mirror.
